public class Pessoal extends Contato{

	private String redeSocial;
	private String celular;

	public void setRedeSocial(String redeSocial){
		this.redeSocial = redeSocial;
	}
	public String getRedeSocial(){
		return redeSocial;
	}

	public void setCelular(String celular){
		this.celular = celular;
	}
	public String getCelular(){
		return celular;
	}
}
